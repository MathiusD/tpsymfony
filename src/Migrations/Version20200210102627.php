<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200210102627 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE recette (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, duree INT NOT NULL, difficulty INT NOT NULL, parts INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ustencile (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ustencile_recette (ustencile_id INT NOT NULL, recette_id INT NOT NULL, INDEX IDX_2EBB3718D054DE4C (ustencile_id), INDEX IDX_2EBB371889312FE9 (recette_id), PRIMARY KEY(ustencile_id, recette_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ustencile_recette ADD CONSTRAINT FK_2EBB3718D054DE4C FOREIGN KEY (ustencile_id) REFERENCES ustencile (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ustencile_recette ADD CONSTRAINT FK_2EBB371889312FE9 FOREIGN KEY (recette_id) REFERENCES recette (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ustencile_recette DROP FOREIGN KEY FK_2EBB371889312FE9');
        $this->addSql('ALTER TABLE ustencile_recette DROP FOREIGN KEY FK_2EBB3718D054DE4C');
        $this->addSql('DROP TABLE recette');
        $this->addSql('DROP TABLE ustencile');
        $this->addSql('DROP TABLE ustencile_recette');
    }
}
