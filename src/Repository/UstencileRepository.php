<?php

namespace App\Repository;

use App\Entity\Ustencile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Ustencile|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ustencile|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ustencile[]    findAll()
 * @method Ustencile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UstencileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ustencile::class);
    }

    // /**
    //  * @return Ustencile[] Returns an array of Ustencile objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Ustencile
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
