<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RecetteRepository")
 */
class Recette
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $duree;

    /**
     * @ORM\Column(type="integer")
     */
    private $difficulty;

    /**
     * @ORM\Column(type="integer")
     */
    private $parts;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Ustencile", mappedBy="use_in")
     */
    private $ustenciles;

    public function __construct()
    {
        $this->ustenciles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDuree(): ?int
    {
        return $this->duree;
    }

    public function setDuree(int $duree): self
    {
        $this->duree = $duree;

        return $this;
    }

    public function getDifficulty(): ?int
    {
        return $this->difficulty;
    }

    public function setDifficulty(int $difficulty): self
    {
        $this->difficulty = $difficulty;

        return $this;
    }

    public function getParts(): ?int
    {
        return $this->parts;
    }

    public function setParts(int $parts): self
    {
        $this->parts = $parts;

        return $this;
    }

    /**
     * @return Collection|Ustencile[]
     */
    public function getUstenciles(): Collection
    {
        return $this->ustenciles;
    }

    public function addUstencile(Ustencile $ustencile): self
    {
        if (!$this->ustenciles->contains($ustencile)) {
            $this->ustenciles[] = $ustencile;
            $ustencile->addUseIn($this);
        }

        return $this;
    }

    public function removeUstencile(Ustencile $ustencile): self
    {
        if ($this->ustenciles->contains($ustencile)) {
            $this->ustenciles->removeElement($ustencile);
            $ustencile->removeUseIn($this);
        }

        return $this;
    }
}
