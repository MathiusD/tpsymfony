<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UstencileRepository")
 */
class Ustencile
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Recette", inversedBy="ustenciles")
     */
    private $use_in;

    public function __construct()
    {
        $this->use_in = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Recette[]
     */
    public function getUseIn(): Collection
    {
        return $this->use_in;
    }

    public function addUseIn(Recette $useIn): self
    {
        if (!$this->use_in->contains($useIn)) {
            $this->use_in[] = $useIn;
        }

        return $this;
    }

    public function removeUseIn(Recette $useIn): self
    {
        if ($this->use_in->contains($useIn)) {
            $this->use_in->removeElement($useIn);
        }

        return $this;
    }
}
